In order to add new analyzer driver:

1. Implement analyzer interfaces from [generic.py](generic.py)
1. Add import function to `drivers` dict at [analyzer.py](analyzer.py)
1. Add driver to `driver:options` at [defaults.yaml](defaults.yaml)
1. Reset program settings
