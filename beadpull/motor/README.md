In order to add new motor driver:

1. Implement motor interfaces from [generic.py](generic.py)
1. Add import function to `drivers` dict at [motor.py](motor.py)
1. Add driver to `driver:options` at [defaults.yaml](defaults.yaml)
1. Reset program settings
